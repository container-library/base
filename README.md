# Base Container Image
> Alpine Linux is a security-oriented, lightweight Linux distribution based on musl libc and busybox. (https://alpinelinux.org/)
>
> Supervisor is a client/server system that allows its users to monitor and control a number of processes on UNIX-like operating systems. (https://supervisord.org/)

Container image as a base for most of the other of our library images: the latest stable version of Alpine Linux together with supervisord as an init system.

## Use it in your Dockerfiles
```
FROM codeberg.org/container-library/base
COPY supervisord.conf /etc/

# Optionally, will be executed before supervisord
COPY init.sh /etc/
```

In the file `supervisord.conf`, you can manage [programs & other options](http://supervisord.org/configuration.html) for supervisord.
