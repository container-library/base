FROM alpine:3.14 AS target

RUN apk add --no-cache supervisor
CMD ["/bin/sh", "-c", "! [ -f /etc/init.sh ] || . /etc/init.sh && exec /usr/bin/supervisord -n -c /etc/supervisord.conf -l /dev/null -y 0 -e info"]

LABEL maintainer="Codeberg Container Library: https://codeberg.org/container-library"
LABEL description="Base image for most other library images: the latest stable version of Alpine Linux with supervisord as an init system."
LABEL version="3.14.0"